#pragma once

#include <string>
#include <atomic>
#include <thread>



struct _ENetPeer; struct _ENetAddress; struct _ENetEvent; struct _ENetHost;
namespace oe::networking {

	class Client {
	private:
		_ENetPeer* m_peer = nullptr;
		_ENetAddress* m_address = nullptr;
		_ENetEvent* m_event = nullptr;
		_ENetHost* m_client = nullptr;
		std::atomic<bool> m_keep_running = false;
		std::thread m_thread;

		void(*m_callback_connect)(Client* client) = nullptr;
		void(*m_callback_disconnect)(Client* client) = nullptr;
		void(*m_callback_recieve)(Client* client, const unsigned char* data, size_t size) = nullptr;

		void operate();

	public:
		Client();
		~Client();

		int connect(std::string ip, int port);
		int disconnect();
		int close();
		int send(const unsigned char* bytes, size_t count);

		void setConnectCallback(void(*callback_connect)(Client* client)) { m_callback_connect = callback_connect; }
		void setDisconnectCallback(void(*callback_disconnect)(Client* client)) { m_callback_disconnect = callback_disconnect; }
		void setReciveCallback(void(*callback_recieve)(Client* client, const unsigned char* data, size_t size)) { m_callback_recieve = callback_recieve; }

	};

}