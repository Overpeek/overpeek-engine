#pragma once

#include <string>
#include <atomic>
#include <thread>
#include <unordered_map>



struct _ENetHost; struct _ENetAddress; struct _ENetPeer;
namespace oe::networking {

	class enet {
	private:
		static bool initialized;

	public:
		static void initEnet();
		static void deinitEnet();
	};

	class Server {
	private:
		_ENetAddress* m_address = nullptr;
		_ENetHost* m_server = nullptr;
		std::atomic<bool> m_keep_running = false;
		std::thread m_thread;
		std::unordered_map<size_t, _ENetPeer*> m_peers = std::unordered_map<size_t, _ENetPeer*>();

		void(*m_callback_connect)(Server* server, size_t client_id) = nullptr;
		void(*m_callback_disconnect)(Server* server, size_t client_id) = nullptr;
		void(*m_callback_recieve)(Server* server, size_t client_id, const unsigned char* data, size_t size) = nullptr;

		void operate();

	public:
		Server();
		~Server();

		int open(std::string ip, int port);
		int close();
		int send(const unsigned char* bytes, size_t count, size_t client_id); // send to specific client
		int send(const unsigned char* bytes, size_t count); // send to all clients

		void setConnectCallback(void(*callback_connect)(Server* server, size_t client_id)) { m_callback_connect = callback_connect; }
		void setDisconnectCallback(void(*callback_disconnect)(Server* server, size_t client_id)) { m_callback_disconnect = callback_disconnect; }
		void setReciveCallback(void(*callback_recieve)(Server* server, size_t client_id, const unsigned char* data, size_t size)) { m_callback_recieve = callback_recieve; }

	};

}