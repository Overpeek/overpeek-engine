#include <engine/engine.h>

#include <string>
#include <thread>



int main(int argc, char* argv[]) {

	int port = 12221;

	// open the server
	auto server = new oe::networking::Server(); 
	server->open("localhost", port);
	server->setConnectCallback([](oe::networking::Server* server, size_t client_id) { spdlog::info("{} connected", client_id); });
	server->setDisconnectCallback([](oe::networking::Server* server, size_t client_id) { spdlog::info("{} disconnected", client_id); });
	server->setReciveCallback([](oe::networking::Server* server, size_t client_id, const unsigned char* data, size_t size) { spdlog::info("recieved {}", (const char*)data); server->send((const unsigned char*)"pong from server", 17, client_id); });
	
	// connect to it with client
	auto client = new oe::networking::Client();
	client->connect("localhost", port);
	client->setReciveCallback([](oe::networking::Client* client, const unsigned char* data, size_t size) { spdlog::info("recieved {}", (const char*)data); });
	client->send((const unsigned char*)"ping from client1", 18);

	// wait for messages to get recieved
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	// close the client
	client->disconnect();
	client->close();

	// close the server
	server->close();

	return 0;
}