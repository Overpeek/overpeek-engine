#include <engine/engine.h>

#include <string>



oe::graphics::SpriteShader* shader;
oe::graphics::Renderer* renderer;
oe::helper::World* world;


void render(float update_fraction) {
	// clear framebuffer
	oe::graphics::Window::clearWindow();

	// submitting
	

	// swap buffers and poll events
	oe::graphics::Window::updateWindow();
	oe::graphics::Window::pollEvents();

	// check if needs to close
	if (oe::graphics::Window::windowShouldClose()) oe::utils::GameLoop::stop();
}

void resize(const glm::vec2& window_size) {
	shader->bind();
	shader->useTexture(false);
	glm::mat4 pr_matrix = glm::perspectiveFov(30.0f, (float)window_size.x, (float)window_size.y, 0.0f, 1000.0f);
	shader->projectionMatrix(pr_matrix);
	glm::mat4 vw_matrix = glm::lookAt(glm::vec3(0.0f, 0.0f, -5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	shader->viewMatrix(vw_matrix);
}

void update() {}

int main(int argc, char* argv[]) {
	// engine
	oe::init(argc, argv);
	world = new oe::helper::World();

	// window
	oe::graphics::Window::WindowConfig window_config;
	window_config.title = "Test 4 - entities";
	window_config.multisamples = 4;
	window_config.opengl_debugmode = true;
	oe::graphics::Window::init(window_config);
	oe::graphics::Window::setResizeCallback(resize);
	oe::graphics::GL::setSwapInterval(1);
	oe::graphics::GL::setBackFaceCulling(false);
	oe::graphics::GL::enableBlending();

	// drawing
	renderer = new oe::graphics::Renderer(oe::graphics::types::dynamicrender, oe::graphics::types::staticrender, 6, nullptr);
	shader = new oe::graphics::SpriteShader();
	resize(oe::graphics::Window::getSize());

	// start
	oe::utils::GameLoop::init(render, update, 1);

	// closing
	oe::graphics::Window::closeWindow();
	delete renderer;
	delete shader;

	return 0;
}
