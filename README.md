# overpeek-engine
Engine project

### Cloning
```
git clone https://gitlab.com/Overpeek/overpeek-engine.git
```

### CMake
```
mkdir build
cd build
cmake ..
```

#### Depends:
- Freetype
- GLFW3
- OpenGL
- OpenAL

### Features
- Window creation (glfw) 
- Audio (OpenAL) 
- Rendering (OpenGL)
    - Text rendering (Freetype) (basic) 
    - GUI (basic)
- General utility tools (that I find useful) 


### TODO
- [ ] Texture packer - rework textures and renderer
- [ ] Freetype -> stb_truetype
- [ ] Optimized text rendering
- [ ] GUI (and ImGUI for debugging) 
- [ ] Easy to use mode
